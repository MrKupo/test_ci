import unittest
import time

ROMPER_BUILD = False

def get_romper_build():
    #Esperamos para darle tiempo a la baliza
    time.sleep(5)
    return ROMPER_BUILD

class Test(unittest.TestCase):
    def test_get_romper_build(self):
        self.assertFalse(get_romper_build())

unittest.main()